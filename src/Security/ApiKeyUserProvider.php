<?php

namespace App\Security;

use App\Repository\UserRepository;
use Firebase\JWT\JWT;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiKeyUserProvider implements UserProviderInterface
{
    /**
     * @var UserRepository
     */
    private $UserRepository;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(UserRepository $UserRepository, ContainerInterface $container)
    {
        $this->UserRepository = $UserRepository;
        $this->container = $container;
    }

    public function getUserByToken($token)
    {
        try {
            $decodedToken = (array)JWT::decode(
                $token,
                $this->container->getParameter('secret'),
                ['HS256']
            );
        } catch (\UnexpectedValueException $e) {
            throw new CustomUserMessageAuthenticationException(
                sprintf('Token "%s" has wrong format.', $token)
            );
        }

        return $this->UserRepository->find($decodedToken['id']);
    }

    public function loadUserByUsername($username)
    {
        throw new UnsupportedUserException();
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
